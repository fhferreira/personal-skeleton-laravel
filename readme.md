Personal Skeleton Laravel
=========

Versão 1.0
----

Esta é uma estrutura básica que foi criada como Skeleton básico para meus projetos. Todas as estruturas estão com o padrão de Namespaces.

Requisitos para funcionamento
----

  - Apache 2.x
  - nginx 1.4.x
  - PHP >= 5.4.x

Módulos implementados ao Skeleton
----
  - [Ochestral Asset 2.1.x]

Módulos de user interface
----
  - [Bootstrap 3.1.1]
  - [Bootstrap Datepicker]
  - [Toast Message]
  - [Chosen 1.1.0]
  - [CKEditor 4.3.4] (É necessário ser adicionado a /app/controllers/BaseController.php)

Módulos pessoais (/app/library/helper)
----
  - __Pagination__ (Gerador de paginação, suportado apenas com Bootstrap 3.x, ainda sem documentação)
  - __Securitycrypt__ (Criptografador e descriptografador de rijndael-256 , ainda sem documentação)
  - __SendRequest__ (Objeto para abstração de cURL para requisitos em WEBSERVICES RESTFUL, ainda sem documentação)
  - __TableGenerator__ (Objeto para criação fácil de tabelas e grids, suportado apenas com Bootstrap 3.x, ainda sem documentação)
  - __WarningBox__ (Criador de mensagens automáticas, suportado apenas com Bootstrap 3.x, ainda sem documentação)

Instalação do composer
----

Para fazer a instalação do [Composer], de forma simples deve ser feito os comandos:

> php composer.phar self-update

> php composer.phar install

Atualização de composer quando é criado um novo arquivo com Namespace
----

Para atualizar o [Composer] quando um novo arquivo com Namespaces é criado basta gerar o comando:

> php composer.phar dump-autoload


[Ochestral Asset 2.1.x]:https://github.com/orchestral/asset
[Bootstrap 3.1.1]:http://getbootstrap.com/
[Bootstrap Datepicker]:http://www.eyecon.ro/bootstrap-datepicker/
[Toast Message]:http://akquinet.github.io/jquery-toastmessage-plugin/
[Chosen 1.1.0]:http://harvesthq.github.io/chosen/
[CKEditor 4.3.4]:http://ckeditor.com/
[Composer]:https://getcomposer.org/


    