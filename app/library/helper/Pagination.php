<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Omicron
 * Date: 24/08/13
 * Time: 17:12
 * To change this template use File | Settings | File Templates.
 */

namespace Helper;


class Pagination
{
    public static function generate($actualPage = null, $totalPage = null)
    {

        if ($actualPage < 0 || $actualPage > $totalPage) {
            return FALSE;
        }

        if (!empty($actualPage) || !empty($totalPage)) {

            $returnPagination = "<ul class=\"pagination\">";

            if ($actualPage == 1) {
                $returnPagination .= "<li class=\"disabled\"><a href=\"javascript:void(0);\">««</a></li>";

                $returnPagination .= "<li class=\"disabled\"><a href=\"javascript:void(0);\">«</a></li>";
            } else {
                $returnPagination .= "<li><a href=\"javascript:void(0);\" page-info=\"1\" class=\"pagination-page\">««</a></li>";

                $returnPagination .= "<li><a href=\"javascript:void(0);\"  page-info=\"" . ($actualPage - 1) . "\" class=\"pagination-page\">«</a></li>";
            }


            for ($countPage = 1; $countPage <= $totalPage; $countPage++) {

                $showValueAfter = 4;

                $showValueBefore = 4;

                if ($countPage < ($actualPage + $showValueAfter) && $countPage > ($actualPage - $showValueBefore)) {

                    if ($actualPage == $countPage) {

                        $returnPagination .= " <li class=\"active\"><a href=\"javascript:void(0);\">" . $countPage . "</a></li>";
                    } else {

                        $returnPagination .= " <li><a href=\"javascript:void(0);\" class=\"pagination-page\" page-info=\"" . $countPage . "\">" . $countPage . "</a></li>";
                    }
                }

            }

            if ($actualPage == $totalPage) {

                $returnPagination .= "<li class=\"disabled\"><a href=\"javascript:void(0);\">»</a></li>";
                $returnPagination .= "<li class=\"disabled\"><a href=\"javascript:void(0);\">»»</a></li>";

            } else {

                $returnPagination .= "<li><a href=\"javascript:void(0);\" page-info=\"" . ($actualPage + 1) . "\" class=\"pagination-page\">»</a></li>";
                $returnPagination .= "<li><a href=\"javascript:void(0);\"  page-info=\"" . $totalPage . "\" class=\"pagination-page\">»»</a></li>";

            }

            $returnPagination .= "</ul>";

            return $returnPagination;
        } else {
            return FALSE;
        }
    }
}