<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Helper;
/**
 * Description of SecurityCrypt
 *
 * @author Omicron
 */
class Securitycrypt {

    /** Encryption Procedure
     *
     *  @param mixed msg message/data
     *  @param string k encryption key
     *  @param boolean base64 base64 encode result
     *
     *  @return string iv+ciphertext+mac or
     * boolean false on error
     */
    public function encrypt($msg, $k, $base64 = false) {


        if (!$td = mcrypt_module_open('rijndael-256', '', 'ofb', '')){
            return false;
        }
        
        $msg = serialize($msg);
        $iv = mcrypt_create_iv(32, MCRYPT_RAND);

        if (mcrypt_generic_init($td, $k, $iv) !== 0){
            return false;
        }

        $msg = mcrypt_generic($td, $msg);
        $msg = $iv . $msg;
        $mac = $this->pbkdf2($msg, $k, 1000, 32);
        $msg .= $mac;

        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        if ($base64){
            $msg = base64_encode($msg);
        }

        return $msg;
    }

    /** Decryption Procedure
     *
     *  @param string msg output from encrypt()
     *  @param string k encryption key
     *  @param boolean base64 base64 decode msg
     *
     *  @return string original message/data or
     * boolean false on error
     */
    public function decrypt($msg, $k, $base64 = false) {

        if ($base64){
            $msg = base64_decode($msg);
        }

        if (!$td = mcrypt_module_open('rijndael-256', '', 'ofb', '')){
            return false;
        }
        
        $iv = substr($msg, 0, 32);
        $mo = strlen($msg) - 32;
        $em = substr($msg, $mo);
        $msg = substr($msg, 32, strlen($msg) - 64);
        $mac = $this->pbkdf2($iv . $msg, $k, 1000, 32);

        if ($em !== $mac){
            return false;
        }

        if (mcrypt_generic_init($td, $k, $iv) !== 0){
            return false;
        }

        $msg = mdecrypt_generic($td, $msg);
        $msg = unserialize($msg);

        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        return $msg;
    }

    /** PBKDF2 Implementation (as described in RFC 2898);
     *
     *  @param string p password
     *  @param string s salt
     *  @param int c iteration count (use 1000 or higher)
     *  @param int kl derived key length
     *  @param string a hash algorithm
     *
     *  @return string derived key
     */
    public function pbkdf2($p, $s, $c, $kl, $a = 'sha256') {

        $hl = strlen(hash($a, null, true));
        $kb = ceil($kl / $hl);
        $dk = '';

        for ($block = 1; $block <= $kb; $block++) {


            $ib = $b = hash_hmac($a, $s . pack('N', $block), $p, true);


            for ($i = 1; $i < $c; $i++){
                $ib ^= ($b = hash_hmac($a, $b, $p, true));
            }

            $dk .= $ib;
        }

        
        return substr($dk, 0, $kl);
    }

}